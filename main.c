#include <stdio.h>
#include <string.h>
#include <secp256k1.h>
#include <stdlib.h>
#include <pthread.h>
#include <openssl/sha.h>

#include "random.h"
#include "sha3.h"
#include "libbase58.h"

int state = 1;
char* prefix;
size_t size_prefix;
int is_prefix = 0;

void sha256_hash(u_int8_t* input, size_t size_input, u_int8_t* output) {
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, input, size_input);
    SHA256_Final(output, &sha256);
}

static void print_hex(unsigned char* data, size_t size) {
    size_t i;

    for (i = 0; i < size; i++) {
        printf("%02x", data[i]);
    }

    printf("\n");
}

void generatePrivateKey(secp256k1_context* ctx, u_int8_t* private_key) {
    while (1) {
        if (!fill_random(private_key, 32)) continue;

        if (secp256k1_ec_seckey_verify(ctx, private_key)) {
            break;
        }

        printf("err");
    }
}

void pubKeyFromPrivateKey(secp256k1_context* ctx, u_int8_t* private_key, u_int8_t* publicKey) {
    int return_val;
    size_t len;
    secp256k1_pubkey pubKey;

    return_val = secp256k1_ec_pubkey_create(ctx, &pubKey, private_key);
    if (!return_val) return;

    len = 65;
    secp256k1_ec_pubkey_serialize(ctx, publicKey, &len, &pubKey, SECP256K1_EC_UNCOMPRESSED);
}

void tronFromPubKey(const u_int8_t* publicKey, char* tronAddress) {
    u_int8_t slicePublicKeyBytes[64];

    for (int i = 0; i < sizeof(slicePublicKeyBytes); i++) {
        slicePublicKeyBytes[i] = publicKey[i+1];
    }

    u_int8_t keccak_hash[32];

    sha3_256(keccak_hash, sizeof(keccak_hash), slicePublicKeyBytes, sizeof(slicePublicKeyBytes));

    u_int8_t slice_keccak_hash[25];

    slice_keccak_hash[0] = 65;

    for (int i = 1; i < 21; i++) {
        slice_keccak_hash[i] = keccak_hash[i+11];
    }

    uint8_t hash[32];

    sha256_hash(slice_keccak_hash, 21, hash);
    sha256_hash(hash, sizeof(hash), hash);

    for (int i = 0; i < 4; ++i) {
        slice_keccak_hash[i + 21] = hash[i];
    }


    size_t b58_size = 35;
    b58enc(tronAddress, &b58_size, slice_keccak_hash, sizeof(slice_keccak_hash));
}

void* threadStart() {
    u_int8_t privateKey[32];
    u_int8_t publicKey[65];
    u_int8_t randomize[32];

    secp256k1_context* ctx = secp256k1_context_create(SECP256K1_CONTEXT_NONE);

    fill_random(randomize, sizeof(randomize));

    while (state) {
        char tron[34];

        generatePrivateKey(ctx, privateKey);
        pubKeyFromPrivateKey(ctx, privateKey, publicKey);

        tronFromPubKey(publicKey, tron);

        int match = 1;

        for (int j = 0; j < size_prefix; j++) {
            if (is_prefix) {
                if (tron[j] != prefix[j]) {
                    match = 0;
                    break;
                }
            } else {
                if (tron[sizeof(tron)-(size_prefix - j)] != prefix[j]) {
                    match = 0;
                    break;
                }
            }
        }

        if (match) {
            state = false;
            printf("%s\n0x", tron);

            for (size_t i = 0; i < sizeof(privateKey); i++) {
                printf("%02x", privateKey[i]);
            }

            printf("\n");
        }
    }
}

int main(int argc, char* argv[]) {
    if (argc != 4) return 1;

    prefix = argv[3];
    size_prefix = strlen(argv[3]);

    if (!strcmp(argv[2], "prefix")) {
        is_prefix = true;
    } else if (!strcmp(argv[2], "postfix")) {
        is_prefix = false;
    } else {

        return 1;
    }

    int threads = atoi(argv[1]);

    pthread_t thread_id[threads];

    for (int i = 0; i < threads; i++) {
        pthread_create(&thread_id[i], NULL, threadStart, NULL);
    }

    for (int i = 0; i < threads; ++i) {
        pthread_join(thread_id[i], NULL);
    }
}
